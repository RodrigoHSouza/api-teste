import apm from 'elastic-apm-node';
import { LogErrorsMongoRepository } from '@/infra/db/monogdb/log-repository/log-errors';
import { LogEntriesMongoRepository } from '@/infra/db/monogdb/log-repository/log-entries';

export function logErrors() {
  return function (
    target: Object,
    key: string | symbol,
    descriptor: PropertyDescriptor,
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = async function (...args) {
      const result = await originalMethod.apply(this, args);

      const logErrorRepository = new LogErrorsMongoRepository();
      if (result.statusCode === 500) {
        apm.captureError(result.body);
        await logErrorRepository.logError(result.body.stack);
      }

      return result;
    };
    return descriptor;
  };
}

export function logEntries(value: string) {
  return function (
    target: Object,
    key: string | symbol,
    descriptor: PropertyDescriptor,
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = async function (...args) {
      const result = await originalMethod.apply(this, args);

      const logEntriesRepository = new LogEntriesMongoRepository();
      await logEntriesRepository.log(value, args[0].body, result);

      return result;
    };
    return descriptor;
  };
}
