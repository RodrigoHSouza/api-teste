import apm from 'elastic-apm-node';
import { traceLabels, TraceOptions } from './trace-protocols';
import searchLabels from '../../search-label';

const getType = (subType: string): string | void => {
  const types = [
    {
      type: 'app',
      subtypes: ['inferred', 'controller', 'graphql', 'mailer', 'resource', 'handler'],
    },
    {
      type: 'db',
      subtypes: ['cassandra', 'cosmosbd', 'db2', 'derby', 'dynamodb', 'elasticsearch', 'graphql', 'h2', 'hsqldb', 'ingres', 'mariadb', 'memcached', 'mongodb', 'mssql', 'mysql', 'oracle', 'postgresql', 'redis', 'sqlite', 'sqlite3', 'sqlserver', 'unknown'],
    },
    {
      type: 'external',
      subtypes: ['dubbo', 'grpc', 'http'],
    },
    {
      type: 'json',
      subtypes: ['parse', 'generate'],
    },
    {
      type: 'messaging',
      subtypes: ['azurequeue', 'azureservicebus', 'jms', 'kafka', 'rabbitmq', 'sns', 'sqs'],
    },
    {
      type: 'storage',
      subtypes: ['azureblob', 'azurefile', 'azuretable', 's3'],
    },
    {
      type: 'websocket',
      subtypes: ['send'],
    },
  ];

  for (const type of types) {
    if (type.subtypes.includes(subType)) return type.type;
  }
};

export function traceMethod(options: TraceOptions, labels?: traceLabels) {
  return function (
    target: Object,
    key: string | symbol,
    descriptor: PropertyDescriptor,
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = async function (...args) {
      const span = apm.startSpan(options.spanName) as apm.Span;

      if (options.subtype) {
        const type = getType(options.subtype);
        if (type) span.type = type;

        span.subtype = options.subtype;
      }

      if (labels) span.addLabels(searchLabels(labels, args));

      const result = await originalMethod.apply(this, args);
      span.end();

      return result;
    };
    return descriptor;
  };
}
