interface TraceObject {
  [label: string]: string | number
}

export type traceLabels = number[] | string[] | TraceObject;

type appSubtype = 'inferred' | 'controller' | 'graphql' | 'mailer' | 'resource' | 'handler';
type dbSubtype = 'cassandra' | 'cosmosbd' | 'db2' | 'derby' | 'dynamodb' | 'elasticsearch' | 'graphql' | 'h2' | 'hsqldb' | 'ingres' | 'mariadb' | 'memcached' | 'mongodb' | 'mssql' | 'mysql' | 'oracle' | 'postgresql' | 'redis' | 'sqlite' | 'sqlite3' | 'sqlserver' | 'unknown';
type externalSubtype = 'dubbo' | 'grpc' | 'http';
type jsonSubtype = 'parse' | 'generate';
type messagingSubtype = 'azurequeue' | 'azureservicebus' | 'jms' | 'kafka' | 'rabbitmq' | 'sns' | 'sqs';
type storageSubtype = 'azureblob' | 'azurefile' | 'azuretable' | 's3';
type websocketSubtype = 'send';

type LiteralUnion<T extends U, U = string> = T | (U & {});

export interface TraceOptions {
  spanName: string
  subtype?: LiteralUnion<
  appSubtype |
  dbSubtype |
  externalSubtype |
  jsonSubtype |
  messagingSubtype |
  storageSubtype |
  websocketSubtype
  >
}
