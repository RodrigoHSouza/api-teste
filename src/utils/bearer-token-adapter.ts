import { GetToken } from '../presentation/protocols/get-token';

export class GetTokenAdapter implements GetToken {
  getToken(data: any): string {
    if (!data.authorization) throw new Error('HEADER_NOT_FOUND');

    const token = data.authorization.split(' ');
    if (token.length < 2 || token[0] !== 'Bearer') throw new Error('INVALID_HEADER');

    return token[1];
  }
}
