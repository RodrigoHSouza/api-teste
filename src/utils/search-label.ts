import { traceLabels } from './decorators/trace/trace-protocols';

const searchLabels = (labels: traceLabels, args: any[]) => Object.entries(args).reduce((acc, [
  key,
  value,
]) => {
  const label = Object.entries(labels).find(([, labelValue]) => labelValue == key);

  if (!label) {
    if (typeof value === 'object' || Array.isArray(value)) { return { ...acc, ...searchLabels(labels, value) }; }
    return acc;
  }

  return { ...acc, [label[0]]: value };
}, {});

export default searchLabels;
