import { UserFake } from '../../../domain/models/user-fake';
import { GetUsersFromFakeApi } from '../../../domain/usecases/get-users-from-fake-api';
import { GetUsersFromFakeApiService } from '../../../infra/http/get-users-from-fake-api-service';

export class HttpUsersFromFakeApi implements GetUsersFromFakeApi {
  private readonly getUsersFromFakeApiService: GetUsersFromFakeApiService

  constructor(getUsersFromFakeApiService: GetUsersFromFakeApiService) {
    this.getUsersFromFakeApiService = getUsersFromFakeApiService;
  }

  async get(): Promise<UserFake[]> {
    return this.getUsersFromFakeApiService.get();
  }
}
