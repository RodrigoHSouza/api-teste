import { RabbitMqServer } from '@/infra/amqp/helpers/amqp-helper';
import { SavePost } from '../../../domain/usecases/save-post';
import { AddPostRespository } from '../../protocols/add-post-repository';

export class DbSavePost implements SavePost {
  private readonly postRepository: AddPostRespository

  constructor(postRepository: AddPostRespository) {
    this.postRepository = postRepository;
  }

  async post(post: any): Promise<any> {
    const postMongo = await this.postRepository.add(post);

    const rabbit = RabbitMqServer.getInstance();

    rabbit.publishInQueue('posts', post, {});

    return postMongo;
  }
}
