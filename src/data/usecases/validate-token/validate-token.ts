import { VerifyToken } from '../../../domain/usecases/verify-token';
import { VerifyInfoAndDecodeToken } from '../../protocols/verify-info-and-decode-token';
import { traceMethod } from '../../../utils/decorators/trace/trace-decorator';

export class ValidateToken implements VerifyToken {
  private readonly verifyInfoAndDecodeToken: VerifyInfoAndDecodeToken

  constructor(verifyInfoAndDecodeToken: VerifyInfoAndDecodeToken) {
    this.verifyInfoAndDecodeToken = verifyInfoAndDecodeToken;
  }

  @traceMethod({ spanName: 'verify-token', subtype: 'encrypt' })
  verify(token: string): any {
    return this.verifyInfoAndDecodeToken.verifyInfoAndDecode(token);
  }
}
