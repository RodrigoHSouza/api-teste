import { AccountModel } from '../../../domain/models/account';
import { UpdateAccount, UpdateAccountModel } from '../../../domain/usecases/update-account';
import { UpdateByIdAccountRepository } from '../../protocols/update-by-id-account-repository';

export class DbUpdateAccount implements UpdateAccount {
  private readonly updateAcocuntRepository: UpdateByIdAccountRepository

  constructor(updateAccountRepository: UpdateByIdAccountRepository) {
    this.updateAcocuntRepository = updateAccountRepository;
  }

  async update(id: string, account: Partial<UpdateAccountModel>): Promise<AccountModel> {
    const updatedAccount = await this.updateAcocuntRepository.updateById(id, account);
    return updatedAccount;
  }
}
