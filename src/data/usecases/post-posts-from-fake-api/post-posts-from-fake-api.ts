import { HttpRequest } from '@/presentation/protocols';
import { PostPostsFromFakeApiService } from '../../../infra/http/post-posts-from-fake-api-service';

export class PostPostsFromFakeApi implements PostPostsFromFakeApi {
  private readonly postPostsFromFakeApiService: PostPostsFromFakeApiService

  constructor(postPostsFromFakeApiService: PostPostsFromFakeApiService) {
    this.postPostsFromFakeApiService = postPostsFromFakeApiService;
  }

  post(request: HttpRequest): Promise<any> {
    return this.postPostsFromFakeApiService.post(request);
  }
}
