import { AccountModel } from '@/domain/models/account';
import { GetUserInfoById } from '../../../domain/usecases/get-user-info-by-id';
import { GetByIdAccountRepository } from '../../protocols/get-by-id-account-repository';

export class DbGetUserInfo implements GetUserInfoById {
  private readonly getByIdAccountRepository: GetByIdAccountRepository

  constructor(getByIdAccountRepository: GetByIdAccountRepository) {
    this.getByIdAccountRepository = getByIdAccountRepository;
  }

  async getUserInfoById(id: string): Promise<AccountModel> {
    const account = await this.getByIdAccountRepository.findById(id);
    return account;
  }
}
