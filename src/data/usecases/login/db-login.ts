import { LoginResponse } from '../../../domain/models/login';
import { HandleLogin, LoginModel } from '../../../domain/usecases/login';
import { ComparePassword } from '../../protocols/compare-password';
import { GetByEmailAccountRepository } from '../../protocols/get-by-email-account-repository';
import { SignToken } from '../../protocols/sign-token';

export class DbLogin implements HandleLogin {
  private readonly token: SignToken

  private readonly accountRepository: GetByEmailAccountRepository

  private readonly comparePassword: ComparePassword

  constructor(
    token: SignToken,
    getByEmailAccountRepository: GetByEmailAccountRepository,
    comparePassword: ComparePassword,
  ) {
    this.token = token;
    this.accountRepository = getByEmailAccountRepository;
    this.comparePassword = comparePassword;
  }

  async login(loginData: LoginModel): Promise<LoginResponse> {
    const account = await this.accountRepository.findByEmail(loginData.email);

    if (account) {
      const {
        id,
        name,
        password,
      } = account;

      const samePassword = await this.comparePassword.comparePassword(loginData.password, password);
      if (samePassword) {
        const token = this.token.sign({ id });
        return { name, token };
      }

      throw new Error('INVALID_PASSWORD');
    }

    throw new Error('ACCOUNT_NOT_FOUND');
  }
}
