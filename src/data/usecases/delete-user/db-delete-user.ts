import { DeleteAccount } from '../../../domain/usecases/delete-account';
import { DeleteByIdRepository } from '../../protocols/delete-account-by-id-repository';

export class DbDeleteUser implements DeleteAccount {
  private readonly deleteByIdRepository: DeleteByIdRepository

  constructor(deleteByIdRepository: DeleteByIdRepository) {
    this.deleteByIdRepository = deleteByIdRepository;
  }

  async delete(id: string): Promise<void> {
    await this.deleteByIdRepository.delete(id);
  }
}
