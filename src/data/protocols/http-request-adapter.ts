import { AxiosRequestConfig } from 'axios';

export type HttpRequest = {
  url: AxiosRequestConfig['url'];
  method: AxiosRequestConfig['method'];
  body?: AxiosRequestConfig['data'];
  headers?: AxiosRequestConfig['headers'];
};

export type HttpResponse = {
  statusCode: number;
  body?: any;
};

export interface HttpClient {
  request: (data: HttpRequest) => Promise<HttpResponse>
}
