export interface SignToken {
  sign: (data: object) => string
}
