import { AccountModel } from '../../domain/models/account';

export interface GetByEmailAccountRepository {
  findByEmail: (email: string) => Promise<AccountModel>
}
