import { AccountModel } from '../../domain/models/account';
import { UpdateAccountModel } from '../../domain/usecases/update-account';

export interface UpdateByIdAccountRepository {
  updateById: (id: string, data: Partial<UpdateAccountModel>) => Promise<AccountModel>
}
