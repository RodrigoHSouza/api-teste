export interface DeleteByIdRepository {
  delete: (id: string) => Promise<void>
}
