export interface GetByIdPostRepository {
  findById: (id: string) => Promise<any>
}