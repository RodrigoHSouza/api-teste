export interface ComparePassword {
  comparePassword: (plainTextPassword: string, hashedPassword: string) => Promise<boolean>
}
