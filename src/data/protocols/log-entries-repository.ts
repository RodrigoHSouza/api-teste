export interface LogEntriesRepository {
  log: (collectionName: string, inData: any, outData: string) => Promise<void>
}
