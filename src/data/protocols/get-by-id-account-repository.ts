import { AccountModel } from '../../domain/models/account';

export interface GetByIdAccountRepository {
  findById: (id: string) => Promise<AccountModel>
}
