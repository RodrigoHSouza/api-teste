import {
  Channel, connect, Connection, Message,
} from 'amqplib';
import { formateCamelCaseKeysForSnakeCase, formateSnakeCaseKeysForCamelCase } from '@badass-team-code/formatted-cases-words';
import apm from 'elastic-apm-node';

type Credentials = {
  user: string;
  password: string;
  host: string;
  port: number;
};

export class RabbitMqServer {
  private connection!: Connection;

  private channel!: Channel;

  private uri!: string;

  private static instance: RabbitMqServer;

  constructor(private credentials: Credentials | null = null) {
    if (this.credentials) this.setCredentials(this.credentials);
  }

  public static getInstance(): RabbitMqServer {
    if (!RabbitMqServer.instance) {
      RabbitMqServer.instance = new RabbitMqServer();
    }

    return RabbitMqServer.instance;
  }

  public setCredentials(credentials: Credentials) {
    this.uri = `amqp://${credentials.user}:${credentials.password}@${credentials.host}:${credentials.port}`;
  }

  public async start() {
    if (this.connection) return;
    if (!this.uri) throw new Error('RABBITMQ_CREDENTIALS_NOT_DEFINED');
    this.connection = await connect(this.uri);
    this.channel = await this.connection.createChannel();
  }

  public async close() {
    if (!this.connection) return;
    await this.connection.close();
  }

  public publishInQueue(queue: string, message: object, headers: object) {
    const span = apm!.startSpan(`RABBITMQ - publish in ${queue}`);

    span!.type = 'messaging';
    span!.subtype = 'rabbitmq';
    span!.addLabels({
      queue,
      message: JSON.stringify(message),
      headers: JSON.stringify(headers),
    });

    const sendedToQueue = this.channel.sendToQueue(
      queue,
      this.messageFromBuffer(formateCamelCaseKeysForSnakeCase(message)),
      {
        headers,
      },
    );

    span!.end();
    return sendedToQueue;
  }

  public publishInExchange(
    exchange: string,
    routingKey: string,
    message: object,
  ) {
    if (!this.connection) throw new Error('CONNECTION_NOT_STARTED');
    return this.channel.publish(
      exchange,
      routingKey,
      this.messageFromBuffer(message),
    );
  }

  private messageFromBuffer(message: any): Buffer {
    const string = JSON.stringify(message);
    return Buffer.from(string);
  }

  private messageToJson(message: Message): object {
    return JSON.parse(message.content.toString());
  }

  public async consume(queue: string, callback: (message: object) => void) {
    if (!this.connection) throw new Error('CONNECTION_NOT_STARTED');
    await this.channel.assertQueue(queue, { durable: false });
    await this.channel.consume(queue, (message) => {
      if (!message) return;

      const transaction = apm.currentTransaction || apm.startTransaction('RABBITMQ');
      const span = transaction!.startSpan(`RABBITMQ - consume ${queue}`);
      span!.type = 'messaging';
      span!.subtype = 'rabbitmq';

      span!.addLabels({ queue, message: message.content.toString() });

      callback(formateSnakeCaseKeysForCamelCase(this.messageToJson(message)));
      this.channel.ack(message);

      span!.end();
      transaction!.end();
    });
  }
}
