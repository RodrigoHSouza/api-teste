import { HttpClient } from '../../data/protocols/http-request-adapter';
import { UserFake } from '../../domain/models/user-fake';
import { GetUsersFromFakeApi } from '../../domain/usecases/get-users-from-fake-api';

export class GetUsersFromFakeApiService implements GetUsersFromFakeApi {
  private readonly httpClient: HttpClient

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  async get(): Promise<UserFake[]> {
    const request = await this.httpClient.request({
      url: 'https://jsonplaceholder.typicode.com/users',
      method: 'GET',
    });

    if (request.statusCode !== 200) throw new Error('REQUEST_FAIL');

    return request.body;
  }
}
