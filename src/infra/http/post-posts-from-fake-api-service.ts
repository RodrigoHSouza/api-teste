import { HttpClient } from '@/data/protocols/http-request-adapter';
import { PostPostsFromFakeApi } from '@/domain/usecases/post-posts-from-fake-api';
import { HttpRequest } from '@/presentation/protocols';

export class PostPostsFromFakeApiService implements PostPostsFromFakeApi {
  private readonly httpClient: HttpClient

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  async post(request: HttpRequest): Promise<any> {
    const requestAxios = await this.httpClient.request({
      url: 'https://jsonplaceholder.typicode.com/posts',
      method: 'POST',
      body: request.body,
      headers: request.headers,
    });

    if (requestAxios.statusCode !== 201) throw new Error('REQUEST_FAIL');

    return requestAxios.body;
  }
}
