import knex from 'knex';

export const sqlConnection = knex({
  client: 'mssql',
  connection: {
    host: 'sql-server',
    port: 1433,
    user: 'sa',
    password: 'Admin123',
    options: {
      encrypt: false,
      enableArithAbort: false,
    },
  },
});
