import { formateCamelCaseKeysForSnakeCase, formateSnakeCaseKeysForCamelCase } from '@badass-team-code/formatted-cases-words';
import { AddAccountRepository } from '../../../../data/protocols/add-account-repository';
import { DeleteByIdRepository } from '../../../../data/protocols/delete-account-by-id-repository';
import { GetByEmailAccountRepository } from '../../../../data/protocols/get-by-email-account-repository';
import { GetByIdAccountRepository } from '../../../../data/protocols/get-by-id-account-repository';
import { UpdateByIdAccountRepository } from '../../../../data/protocols/update-by-id-account-repository';
import { AccountModel } from '../../../../domain/models/account';
import { AddAccountModel } from '../../../../domain/usecases/add-account';
import { UpdateAccountModel } from '../../../../domain/usecases/update-account';
import { sqlConnection } from '../helpers/mssql-helper';

export class AccountSQLRepository
implements
  AddAccountRepository,
  GetByIdAccountRepository,
  GetByEmailAccountRepository,
  UpdateByIdAccountRepository,
  DeleteByIdRepository {
  async add(accountData: AddAccountModel): Promise<AccountModel> {
    accountData = formateCamelCaseKeysForSnakeCase(accountData);

    const addResult = await sqlConnection(
      '[api-teste].[dbo].[accounts]',
    ).insert(accountData)
      .returning('id');

    return this.findById(addResult[0]);
  }

  async findById(id: string): Promise<AccountModel> {
    const account = await sqlConnection(
      '[api-teste].[dbo].[accounts]',
    ).select('*')
      .where('id', '=', id);

    if (!account.length) throw new Error('ACCOUNT_NOT_FOUND');
    return formateSnakeCaseKeysForCamelCase(account[0]);
  }

  async findByEmail(email: string) : Promise<AccountModel> {
    const account = await sqlConnection(
      '[api-teste].[dbo].[accounts]',
    ).select('*')
      .where('email', '=', email);

    if (!account.length) throw new Error('ACCOUNT_NOT_FOUND');
    return formateSnakeCaseKeysForCamelCase(account[0]);
  }

  async updateById(id: string, data: Partial<UpdateAccountModel>): Promise<AccountModel> {
    data = formateCamelCaseKeysForSnakeCase(data);

    const updateResult = await sqlConnection(
      '[api-teste].[dbo].[accounts]',
    ).update({ ...data })
      .where('id', '=', id)
      .returning('id');

    return this.findById(updateResult[0]);
  }

  async delete(id: string): Promise<void> {
    const deleteResult = await sqlConnection(
      '[api-teste].[dbo].[accounts]',
    ).where('id', '=', id)
      .del();

    if (deleteResult < 1) throw new Error('ACCOUNT_NOT_FOUND');
  }
}
