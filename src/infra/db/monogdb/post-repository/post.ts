import { formateCamelCaseKeysForSnakeCase, formateSnakeCaseKeysForCamelCase } from '@badass-team-code/formatted-cases-words';
import { traceMethod } from '@/utils/decorators/trace/trace-decorator';
import { AddPostRespository } from '../../../../data/protocols/add-post-repository';
import { GetByIdPostRepository } from '../../../../data/protocols/get-by-id-post-repository';
import { MongoHelper } from '../helpers/mongo-helper';

export class PostRepository implements AddPostRespository, GetByIdPostRepository {
  @traceMethod({ spanName: 'posts [findById]', subtype: 'mongodb' }, { id: 0 })
  async findById(id: string): Promise<any> {
    const postCollection = await MongoHelper.getCollection('posts');
    const post = await postCollection.findOne({ _id: id });

    if (!post) throw new Error('POST_NOT_FOUND');
    return MongoHelper.map(formateSnakeCaseKeysForCamelCase(post));
  }

  @traceMethod({ spanName: 'posts [add]', subtype: 'mongodb' })
  async add(post: any): Promise<any> {
    post = formateCamelCaseKeysForSnakeCase(post);

    const postCollection = await MongoHelper.getCollection('posts');
    const { insertedId } = await postCollection.insertOne(post);

    const postMongo = await this.findById(insertedId);
    return postMongo;
  }
}
