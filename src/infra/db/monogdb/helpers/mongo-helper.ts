import { Collection, MongoClient } from 'mongodb';

export const MongoHelper = {
  client: MongoClient,
  uri: null as any as string,

  async connect(uri: string) {
    this.uri = uri;
    this.client = await MongoClient.connect(uri, {
      useUnifiedTopology: true,
    });
  },

  async disconnect() {
    await this.client.close();
    this.client = null;
  },

  async getCollection(name: string): Promise<Collection> {
    if (!this.client?.topology?.isConnected()) {
      await this.connect(this.uri);
    }

    return this.client.db().collection(name);
  },

  map(collection): any {
    const { _id, ...collectionWithoutId } = collection;
    return { ...collectionWithoutId, ...{ id: _id } };
  },
};
