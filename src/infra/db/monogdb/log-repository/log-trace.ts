import { formateCamelCaseKeysForSnakeCase } from '@badass-team-code/formatted-cases-words';
import { Transaction } from 'elastic-apm-node';
import { LogTraceRepository } from '../../../../data/protocols/log-trace-repository';
import { MongoHelper } from '../helpers/mongo-helper';

export class LogTraceMongoRepository implements LogTraceRepository {
  async log(transaction: Transaction, data: any): Promise<void> {
    const collection = await MongoHelper.getCollection('trace');
    await collection.insertOne(formateCamelCaseKeysForSnakeCase({
      name: transaction.name,
      result: transaction.result,
      labels: data,
      date: new Date(),
    }));
  }
}
