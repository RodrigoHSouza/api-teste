import { formateCamelCaseKeysForSnakeCase } from '@badass-team-code/formatted-cases-words';
import { LogErrorRepository } from '../../../../data/protocols/log-error-repository';
import { MongoHelper } from '../helpers/mongo-helper';

export class LogErrorsMongoRepository implements LogErrorRepository {
  async logError(stack: string): Promise<void> {
    const errorCollention = await MongoHelper.getCollection('errors');
    await errorCollention.insertOne(formateCamelCaseKeysForSnakeCase({
      stack,
      date: new Date(),
    }));
  }
}
