import { formateCamelCaseKeysForSnakeCase } from '@badass-team-code/formatted-cases-words';
import { LogEntriesRepository } from '../../../../data/protocols/log-entries-repository';
import { MongoHelper } from '../helpers/mongo-helper';

export class LogEntriesMongoRepository implements LogEntriesRepository {
  async log(collectionName: string, inData: any, outData: string): Promise<void> {
    const collection = await MongoHelper.getCollection(collectionName);
    await collection.insertOne(formateCamelCaseKeysForSnakeCase({
      inData,
      outData,
      date: new Date(),
    }));
  }
}
