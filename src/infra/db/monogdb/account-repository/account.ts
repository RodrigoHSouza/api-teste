import { formateCamelCaseKeysForSnakeCase, formateSnakeCaseKeysForCamelCase } from '@badass-team-code/formatted-cases-words';
import { ObjectId } from 'bson';
import { traceMethod } from '@/utils/decorators/trace/trace-decorator';
import { AddAccountRepository } from '../../../../data/protocols/add-account-repository';
import { DeleteByIdRepository } from '../../../../data/protocols/delete-account-by-id-repository';
import { GetByEmailAccountRepository } from '../../../../data/protocols/get-by-email-account-repository';
import { GetByIdAccountRepository } from '../../../../data/protocols/get-by-id-account-repository';
import { UpdateByIdAccountRepository } from '../../../../data/protocols/update-by-id-account-repository';
import { AccountModel } from '../../../../domain/models/account';
import { AddAccountModel } from '../../../../domain/usecases/add-account';
import { UpdateAccountModel } from '../../../../domain/usecases/update-account';
import { MongoHelper } from '../helpers/mongo-helper';

export class AccountMongoRepository
implements
    AddAccountRepository,
    GetByEmailAccountRepository,
    GetByIdAccountRepository,
    UpdateByIdAccountRepository,
    DeleteByIdRepository {
  @traceMethod({ spanName: 'MONGODB - delete', subtype: 'mongodb' }, { id: 0 })
  async delete(id: string): Promise<void> {
    const accountCollection = await MongoHelper.getCollection('accounts');
    const deleteResult = await accountCollection.deleteOne({ _id: new ObjectId(id) });

    if (deleteResult.deletedCount < 1) throw new Error('ACCOUNT_NOT_FOUND');
  }

  @traceMethod({ spanName: 'accounts [updateById]', subtype: 'mongodb' }, { id: 0 })
  async updateById(id: string, data: Partial<UpdateAccountModel>): Promise<AccountModel> {
    data = formateCamelCaseKeysForSnakeCase(data);

    const accountCollection = await MongoHelper.getCollection('accounts');
    const updateResult = await accountCollection.updateOne(
      { _id: new ObjectId(id) },
      { $set: data },
    );

    if (updateResult.modifiedCount < 1) throw new Error('ACCOUNT_NOT_FOUND');
    return this.findById(id);
  }

  @traceMethod({ spanName: 'accounts [findById]', subtype: 'mongodb' }, { id: 0 })
  async findById(id: string): Promise<AccountModel> {
    const accountCollection = await MongoHelper.getCollection('accounts');
    const account = await accountCollection.findOne({ _id: new ObjectId(id) });

    if (!account) throw new Error('ACCOUNT_NOT_FOUND');
    return MongoHelper.map(formateSnakeCaseKeysForCamelCase(account));
  }

  @traceMethod({ spanName: 'accounts [add]', subtype: 'mongodb' })
  async add(accountData: AddAccountModel): Promise<AccountModel> {
    accountData = formateCamelCaseKeysForSnakeCase(accountData);

    const accountCollection = await MongoHelper.getCollection('accounts');
    const { insertedId } = await accountCollection.insertOne(accountData);

    return this.findById(insertedId.id.toString());
  }

  @traceMethod({ spanName: 'accounts [findByEmail]', subtype: 'mongodb' }, { email: 0 })
  async findByEmail(email: string): Promise<AccountModel> {
    const accountCollection = await MongoHelper.getCollection('accounts');
    const account = await accountCollection.findOne({ email });

    if (!account) throw new Error('ACCOUNT_NOT_FOUND');
    return MongoHelper.map(formateSnakeCaseKeysForCamelCase(account));
  }
}
