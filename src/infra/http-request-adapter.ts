import { AxiosInstance } from 'axios';
import apm from 'elastic-apm-node';
import { HttpClient, HttpRequest, HttpResponse } from '../data/protocols/http-request-adapter';

export class RequestAdapter implements HttpClient {
  private readonly axios: AxiosInstance

  constructor(axiosInstance: AxiosInstance) {
    this.axios = axiosInstance;
  }

  private makeObjFromHeaders(labelName: string, headers: any) {
    return Object.entries(headers || {}).reduce((acc, [key, value]) => ({ ...acc, [`${labelName}-${key}`]: JSON.stringify(value) }), {});
  }

  async request(data: HttpRequest): Promise<HttpResponse> {
    const axiosResponse = await this.axios({
      data: data.body,
      method: data.method,
      url: data.url,
    });

    const span = apm.currentSpan;
    span!.addLabels({ 'request-body': JSON.stringify(data.body || {}) });
    span!.addLabels({ 'request-endpoint': new URL(data.url!).pathname });

    span!.addLabels({ 'response-statusCode': axiosResponse.status });
    span!.addLabels({ 'respose-body': JSON.stringify(axiosResponse.data || {}) });

    span!.addLabels(this.makeObjFromHeaders('request-headers', data.headers));
    span!.addLabels(this.makeObjFromHeaders('response-headers', axiosResponse.headers));

    return {
      statusCode: axiosResponse.status,
      body: axiosResponse.data,
    };
  }
}
