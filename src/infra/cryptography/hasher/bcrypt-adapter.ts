import bcrypt from 'bcrypt';
import { traceMethod } from '../../../utils/decorators/trace/trace-decorator';
import { ComparePassword } from '../../../data/protocols/compare-password';
import { Encrypter } from '../../../data/protocols/encrypter';

export class BcryptAdapter implements Encrypter, ComparePassword {
  private readonly salt: number

  constructor(salt: number) {
    this.salt = salt;
  }

  @traceMethod({ spanName: 'encrypt-password', subtype: 'encrypt' })
  async encrypt(value: string): Promise<string> {
    const hash = await bcrypt.hash(value, this.salt);
    return hash;
  }

  @traceMethod({ spanName: 'compare-password', subtype: 'encrypt' }, { pwd: 0, hashed: 1 })
  async comparePassword(plainTextPassword: string, hashedPassword: string): Promise<boolean> {
    const samePassword = await bcrypt.compare(plainTextPassword, hashedPassword);
    return samePassword;
  }
}
