import jwt from 'jsonwebtoken';
import { SignToken } from '../../../data/protocols/sign-token';
import { VerifyInfoAndDecodeToken } from '../../../data/protocols/verify-info-and-decode-token';

export class JwtAdapter implements SignToken, VerifyInfoAndDecodeToken {
  private readonly secreKey: string

  constructor(secretKey: string) {
    this.secreKey = secretKey;
  }

  verifyInfoAndDecode(token: string): any {
    const decodedToken = jwt.verify(token, this.secreKey);
    return decodedToken;
  }

  sign(data: object): string {
    return jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (60 * 60),
      data,
    }, this.secreKey);
  }
}
