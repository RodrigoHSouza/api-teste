import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { adaptRouteMiddleware } from '../adapters/middleware-route-adapter';
import { makeDeleteUserController } from '../factories/controllers/make-delete-user';
import { MakeVerifyAdminMiddleware } from '../factories/middlewares/make-verify-admin';
import { makeVerifyTokenMiddleware } from '../factories/middlewares/make-verify-token';

export default (router: Router): void => {
  router.delete('/delete_user', adaptRouteMiddleware(makeVerifyTokenMiddleware()), adaptRouteMiddleware(MakeVerifyAdminMiddleware()), adaptRouteController(makeDeleteUserController()));
};
