import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { makeLoginController } from '../factories/controllers/make-login';

export default (router: Router): void => {
  router.post('/login', adaptRouteController(makeLoginController()));
};
