import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { adaptRouteMiddleware } from '../adapters/middleware-route-adapter';
import { makeGetUserInfoController } from '../factories/controllers/make-get-user-info';
import { makeVerifyTokenMiddleware } from '../factories/middlewares/make-verify-token';

export default (router: Router): void => {
  router.get('/user_info', adaptRouteMiddleware(makeVerifyTokenMiddleware()), adaptRouteController(makeGetUserInfoController()));
};
