import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { makeApiOnlineController } from '../factories/controllers/make-api-online';

export default (router: Router): void => {
  router.get('/', adaptRouteController(makeApiOnlineController()));
};
