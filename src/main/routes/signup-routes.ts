import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { makeSignUpController } from '../factories/controllers/make-signup';

export default (router: Router): void => {
  router.post('/signup', adaptRouteController(makeSignUpController()));
};
