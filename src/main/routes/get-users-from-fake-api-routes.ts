import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { makeGetUsersFromFakeApi } from '../factories/controllers/make-get-users-from-fake-api';

export default (router: Router): void => {
  router.get('/fake_users', adaptRouteController(makeGetUsersFromFakeApi()));
};
