import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { makePostPostsFromFakeApi } from '../factories/controllers/make-post-posts-from-fake-api';

export default (router: Router): void => {
  router.post('/fake_posts', adaptRouteController(makePostPostsFromFakeApi()));
};
