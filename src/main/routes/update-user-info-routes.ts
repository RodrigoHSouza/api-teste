import { Router } from 'express';
import { adaptRouteController } from '../adapters/controller-route-adapter';
import { adaptRouteMiddleware } from '../adapters/middleware-route-adapter';
import { makeUpdateUserInfo } from '../factories/controllers/make-update-user-info';
import { makeVerifyTokenMiddleware } from '../factories/middlewares/make-verify-token';

export default (router: Router): void => {
  router.put('/user', adaptRouteMiddleware(makeVerifyTokenMiddleware()), adaptRouteController(makeUpdateUserInfo()));
};
