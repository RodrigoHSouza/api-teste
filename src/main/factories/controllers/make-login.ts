import { DbLogin } from '../../../data/usecases/login/db-login';
import { BcryptAdapter } from '../../../infra/cryptography/hasher/bcrypt-adapter';
import { JwtAdapter } from '../../../infra/cryptography/token/jwt-adapter';
import { LoginController } from '../../../presentation/controllers/login/login';
import { EmailValidatorAdapter } from '../../../utils/email-validator-adapter';
import env from '../../config/env';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';

export const makeLoginController = (): LoginController => {
  const token = new JwtAdapter(env.secreKeyJwt);
  const accountMongoRepository = new AccountSQLRepository();
  const bcryptAdapter = new BcryptAdapter(12);
  const login = new DbLogin(token, accountMongoRepository, bcryptAdapter);

  const emailValidator = new EmailValidatorAdapter();
  return new LoginController(emailValidator, login);
};
