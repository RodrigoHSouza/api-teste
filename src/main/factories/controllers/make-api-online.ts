import { ApiOnlineController } from '../../../presentation/controllers/api-online/api-online';

export const makeApiOnlineController = () => {
  const apiOnlineController = new ApiOnlineController();
  return apiOnlineController;
};
