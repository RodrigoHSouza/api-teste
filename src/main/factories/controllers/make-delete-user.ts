import { DbDeleteUser } from '../../../data/usecases/delete-user/db-delete-user';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';
import { DeleteUserController } from '../../../presentation/controllers/delete-user/delete-user';

export const makeDeleteUserController = (): DeleteUserController => {
  const accountRepository = new AccountSQLRepository();
  const deleteAccount = new DbDeleteUser(accountRepository);

  const deleteUserController = new DeleteUserController(deleteAccount);
  return deleteUserController;
};
