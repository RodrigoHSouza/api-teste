import { DbUpdateAccount } from '../../../data/usecases/update-account/db-update-account';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';
import { UpdateUserInfoController } from '../../../presentation/controllers/update-user-info/update-user-info';

export const makeUpdateUserInfo = (): UpdateUserInfoController => {
  const accountRepository = new AccountSQLRepository();
  const dbUpdateAccount = new DbUpdateAccount(accountRepository);

  const updateUserInfoController = new UpdateUserInfoController(dbUpdateAccount);
  return updateUserInfoController;
};
