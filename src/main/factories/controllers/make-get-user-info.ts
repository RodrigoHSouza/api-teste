import { DbGetUserInfo } from '../../../data/usecases/get-user-info/db-get-user-info';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';
import { GetUserInfoController } from '../../../presentation/controllers/get-user-info/get-user-info';

export const makeGetUserInfoController = (): GetUserInfoController => {
  const getByIdAccountRepository = new AccountSQLRepository();
  const getUserInfo = new DbGetUserInfo(getByIdAccountRepository);

  const getUserInfoController = new GetUserInfoController(getUserInfo);
  return getUserInfoController;
};
