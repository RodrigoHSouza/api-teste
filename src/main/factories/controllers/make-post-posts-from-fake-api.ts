import axios from 'axios';
import { RequestAdapter } from '../../../infra/http-request-adapter';
import { PostPostsFromFakeApiController } from '../../../presentation/controllers/post-posts-from-fake-api/post-posts-from-fake-api';
import { PostPostsFromFakeApiService } from '../../../infra/http/post-posts-from-fake-api-service';
import { DbSavePost } from '../../../data/usecases/save-post/db-save-post';
import { PostRepository } from '../../../infra/db/monogdb/post-repository/post';

export const makePostPostsFromFakeApi = (): PostPostsFromFakeApiController => {
  const axiosInstance = axios;
  const requestAdapter = new RequestAdapter(axiosInstance);
  const postPostsFromFakeApi = new PostPostsFromFakeApiService(requestAdapter);

  const postRepository = new PostRepository();
  const dbSavePost = new DbSavePost(postRepository);

  const postPostsFromFakeApiController = new PostPostsFromFakeApiController(
    postPostsFromFakeApi,
    dbSavePost,
  );
  return postPostsFromFakeApiController;
};
