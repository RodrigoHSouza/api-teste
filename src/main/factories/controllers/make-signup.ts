import { DbAddAccount } from '../../../data/usecases/add-account/db-add-account';
import { BcryptAdapter } from '../../../infra/cryptography/hasher/bcrypt-adapter';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';
import { SignUpController } from '../../../presentation/controllers/signup/signup';
import { EmailValidatorAdapter } from '../../../utils/email-validator-adapter';

export const makeSignUpController = (): SignUpController => {
  const bcryptAdapter = new BcryptAdapter(12);
  const accountRepository = new AccountSQLRepository();
  const dbAddAccount = new DbAddAccount(bcryptAdapter, accountRepository);

  const emailValidatorAdapter = new EmailValidatorAdapter();
  return new SignUpController(emailValidatorAdapter, dbAddAccount);
};
