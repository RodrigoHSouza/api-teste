import axios from 'axios';
import { RequestAdapter } from '../../../infra/http-request-adapter';
import { GetUsersFromFakeApiService } from '../../../infra/http/get-users-from-fake-api-service';
import { GetUsersFromFakeApiController } from '../../../presentation/controllers/get-users-from-fake-api/get-users-from-fake-api';

export const makeGetUsersFromFakeApi = (): GetUsersFromFakeApiController => {
  const axiosInstance = axios;
  const requestAdapter = new RequestAdapter(axiosInstance);
  const getUsersFromFakeApi = new GetUsersFromFakeApiService(requestAdapter);

  const getUsersFromFakeApiController = new GetUsersFromFakeApiController(getUsersFromFakeApi);
  return getUsersFromFakeApiController;
};
