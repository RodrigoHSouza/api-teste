import { ValidateToken } from '../../../data/usecases/validate-token/validate-token';
import { JwtAdapter } from '../../../infra/cryptography/token/jwt-adapter';
import { VerifyTokenMiddleware } from '../../../presentation/middlewares/verify-token';
import { GetTokenAdapter } from '../../../utils/bearer-token-adapter';
import env from '../../config/env';

export const makeVerifyTokenMiddleware = (): VerifyTokenMiddleware => {
  const jwtAdapter = new JwtAdapter(env.secreKeyJwt);
  const verifyToken = new ValidateToken(jwtAdapter);

  const getToken = new GetTokenAdapter();
  const verifyTokenMiddleware = new VerifyTokenMiddleware(getToken, verifyToken);
  return verifyTokenMiddleware;
};
