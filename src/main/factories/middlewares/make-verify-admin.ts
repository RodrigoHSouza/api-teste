import { DbGetUserInfo } from '../../../data/usecases/get-user-info/db-get-user-info';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';
import { VerifyAdminMiddleware } from '../../../presentation/middlewares/verify-admin';

export const MakeVerifyAdminMiddleware = (): VerifyAdminMiddleware => {
  const getByIdAccountRepository = new AccountSQLRepository();
  const getUserInfo = new DbGetUserInfo(getByIdAccountRepository);

  const verifyAdminMiddleware = new VerifyAdminMiddleware(getUserInfo);
  return verifyAdminMiddleware;
};
