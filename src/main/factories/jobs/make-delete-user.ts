import { DeleteUserJob } from '../../jobs/delete-user';
import { DbDeleteUser } from '../../../data/usecases/delete-user/db-delete-user';
import { AccountSQLRepository } from '../../../infra/db/mssql/account-repository/account';

export const makeDeleteUserJob = (): DeleteUserJob => {
  const accountRepository = new AccountSQLRepository();
  const deleteAccount = new DbDeleteUser(accountRepository);

  const deleteUserJob = new DeleteUserJob(deleteAccount);
  return deleteUserJob;
};
