import { Job } from '../protocols/listener-job';
import { DeleteAccount } from '../../domain/usecases/delete-account';
import { logger } from '../config/logger';

export class DeleteUserJob implements Job {
  private deleteAccount: DeleteAccount

  constructor(deleteAccount: DeleteAccount) {
    this.deleteAccount = deleteAccount;
  }

  async handle(message: any): Promise<void> {
    try {
      if (!message.id) return;
      await this.deleteAccount.delete(message.id);
    } catch (error) {
      switch (error.message) {
        default:
          logger.log('error', error);
      }
    }
  }
}
