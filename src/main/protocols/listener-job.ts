export interface Job {
  handle: (message: any) => Promise<void>
}
