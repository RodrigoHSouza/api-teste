export type Listener = {
  enable: boolean;
  queue: string;
  handle: (message: object) => void;
};
