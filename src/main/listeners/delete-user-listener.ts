import { listenerJobAdapter } from '../adapters/listener-job-adapter';
import { Listener } from '../protocols/listener';
import { makeDeleteUserJob } from '../factories/jobs/make-delete-user';

const deleteUserListener: Listener = {
  enable: true,
  queue: 'delete-account',
  handle: listenerJobAdapter(makeDeleteUserJob()),
};

export { deleteUserListener };
