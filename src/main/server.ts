import apm from 'elastic-apm-node';
import { RabbitMqServer } from '@/infra/amqp/helpers/amqp-helper';
import { MongoHelper } from '../infra/db/monogdb/helpers/mongo-helper';
import { sqlConnection } from '../infra/db/mssql/helpers/mssql-helper';
import app from './config/app';
import env from './config/env';
import setupListener from './config/listener-worker';

(async () => {
  apm.start({
    serviceName: 'api-teste',
    secretToken: 'ztd7YTeXknlj169Kqy',
    serverUrl: 'https://0056c57e9255412bb3ea517b174ca95c.apm.southamerica-east1.gcp.elastic-cloud.com:443',
    environment: 'development',
    active: true,
    captureBody: 'all',
    captureHeaders: true,
    captureErrorLogStackTraces: 'always',
    captureExceptions: true,
    asyncHooks: true,
    logLevel: 'off',
    logUncaughtExceptions: true,
    instrument: true,
    instrumentIncomingHTTPRequests: true,
    captureSpanStackTraces: true,
  });

  await MongoHelper.connect(env.mongoUrl);

  const rabbitMqServer = RabbitMqServer.getInstance();
  rabbitMqServer.setCredentials({
    user: 'guest',
    password: 'guest',
    host: 'rabbitmq',
    port: 5672,
  });
  await rabbitMqServer.start();

  setupListener(rabbitMqServer);

  await sqlConnection.raw('SELECT 1');

  // eslint-disable-next-line no-console
  app.listen(env.port, () => console.log(`LISTEN ${env.port}`));
})();
