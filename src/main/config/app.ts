import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import setupRoutes from './routes';

const app = express();

app.use(cors({ exposedHeaders: 'X-Total-Count' }));
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

setupRoutes(app);

export default app;
