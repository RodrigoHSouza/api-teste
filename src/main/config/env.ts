export default {
  mongoUrl: process.env.MONGO_URL || 'mongodb://sa:Admin123@mongo-db:27017/admin',
  port: process.env.PORT || 3000,
  secreKeyJwt: 'super_secret_key',
};
