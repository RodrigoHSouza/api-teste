import { Request, Response } from 'express';
import { Controller, HttpRequest } from '../../presentation/protocols';

export const adaptRouteController = (
  controller: Controller,
) => async (req: Request, res: Response): Promise<void> => {
  const httpResquest: HttpRequest = {
    headers: req.headers,
    body: req.body,
    query: req.query,
    url: req.url,
  };
  const httpResponse = await controller.handle(httpResquest);

  res
    .status(httpResponse.statusCode)
    .json(httpResponse.body);
};
