import { NextFunction, Request, Response } from 'express';
import { Middleware } from '../../presentation/protocols/middleware';

export const adaptRouteMiddleware = (
  middleware: Middleware,
) => async (req: Request, res: Response, next: NextFunction) => {
  const httpResponse = await middleware.handle(req, next);

  if (!httpResponse) return;

  if (httpResponse.headers) res.set(httpResponse.headers);

  return res
    .status(httpResponse.statusCode)
    .json(httpResponse.body);
};
