import { Job } from '../protocols/listener-job';

export const listenerJobAdapter = (job: Job) => async (message: object) => {
  await job.handle(message);
};
