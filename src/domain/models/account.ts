export interface AccountModel {
  id: string
  name: string
  email: string
  password: string
  admin: boolean
}
