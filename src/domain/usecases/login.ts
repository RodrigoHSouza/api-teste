import { LoginResponse } from '../models/login';

export interface LoginModel {
  email: string
  password: string
}

export interface HandleLogin {
  login: (loginData: LoginModel) => Promise<LoginResponse>
}
