import { AccountModel } from '../models/account';

export interface UpdateAccountModel {
  name: string
  email: string
}

export interface UpdateAccount {
  update: (id: string, account: Partial<UpdateAccountModel>) => Promise<AccountModel>
}
