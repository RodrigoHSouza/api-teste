import { HttpRequest } from '@/presentation/protocols';

export interface PostPostsFromFakeApi {
  post: (request: HttpRequest) => Promise<any>
}
