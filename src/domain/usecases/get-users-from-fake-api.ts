import { UserFake } from '../models/user-fake';

export interface GetUsersFromFakeApi {
  get: () => Promise<UserFake[]>
}
