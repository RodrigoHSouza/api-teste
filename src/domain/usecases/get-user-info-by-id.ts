import { AccountModel } from '../models/account';

export interface GetUserInfoById {
  getUserInfoById: (id: string) => Promise<AccountModel>
}
