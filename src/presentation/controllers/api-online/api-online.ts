import { Controller, HttpResponse } from '../../protocols';

export class ApiOnlineController implements Controller {
  handle(): Promise<HttpResponse> {
    return new Promise((resolve) => resolve({
      statusCode: 200,
      body: 'API ONLINE',
    }));
  }
}
