import { GetUserInfoById } from '../../../domain/usecases/get-user-info-by-id';
import { NotLoggedIn } from '../../errors';
import { ok, serverError, unauthorized } from '../../helpers/http-helper';
import { Controller, HttpRequest, HttpResponse } from '../../protocols';
import { logErrors } from '@/utils/decorators/logger-decorator';
import { logger } from '@/main/config/logger';

export class GetUserInfoController implements Controller {
  private readonly getUserInfo: GetUserInfoById

  constructor(getUserInfo: GetUserInfoById) {
    this.getUserInfo = getUserInfo;
  }

  @logErrors()
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const requiredHeaders = [
        'authorization',
      ];

      for (const field of requiredHeaders) {
        if (!httpRequest.headers[field]) {
          return unauthorized(new NotLoggedIn());
        }
      }

      const {
        password,
        ...userInfo
      } = await this.getUserInfo.getUserInfoById(httpRequest.body.token.id);

      return ok(userInfo);
    } catch (error) {
      switch (error.message) {
        case 'ACCOUNT_NOT_FOUND': {
          logger.log('warn', `Account not found: ${httpRequest.query.id}`);
          return unauthorized(new NotLoggedIn());
        }
        default: {
          logger.log('error', error);
          return serverError(error);
        }
      }
    }
  }
}
