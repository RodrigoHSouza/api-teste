import { logEntries, logErrors } from '@/utils/decorators/logger-decorator';
import { InvalidParamError, MissingParamErro } from '../../errors';
import { badRequest, ok, serverError } from '../../helpers/http-helper';
import {
  AddAccount, Controller, EmailValidator, HttpRequest, HttpResponse,
} from './signup-protocols';
import { logger } from '@/main/config/logger';

export class SignUpController implements Controller {
  private readonly emailValidator: EmailValidator

  private readonly addAccount: AddAccount

  constructor(emailValidator: EmailValidator, addAccount: AddAccount) {
    this.emailValidator = emailValidator;
    this.addAccount = addAccount;
  }

  @logErrors()
  @logEntries('signups')
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const requiredFields = [
        'name',
        'email',
        'password',
        'passwordConfirmation',
      ];

      for (const field of requiredFields) {
        if (!httpRequest.body[field]) {
          return badRequest(new MissingParamErro(field));
        }
      }

      const {
        name, email, password, passwordConfirmation,
      } = httpRequest.body;

      if (password !== passwordConfirmation) {
        return badRequest(new InvalidParamError('passwordConfirmation'));
      }

      if (!this.emailValidator.isValid(email)) {
        return badRequest(new InvalidParamError('email'));
      }

      const account = await this.addAccount.add({
        name, email, password, admin: false,
      });

      logger.log('info', `Account created: ${account.id}`);
      return ok(account);
    } catch (error) {
      logger.log('error', error);
      return serverError(error);
    }
  }
}
