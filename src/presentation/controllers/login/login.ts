import { logErrors } from '@/utils/decorators/logger-decorator';
import { HandleLogin } from '../../../domain/usecases/login';
import { InvalidParamError, InvalidPasswordOrEmail, MissingParamErro } from '../../errors';
import {
  badRequest, ok, serverError, unauthorized,
} from '../../helpers/http-helper';
import { Controller, HttpRequest, HttpResponse } from '../../protocols';
import { EmailValidator } from '../signup/signup-protocols';
import { logger } from '@/main/config/logger';

export class LoginController implements Controller {
  private readonly emailValidator: EmailValidator

  private readonly handleLogin: HandleLogin

  constructor(emailValidator: EmailValidator, handleLogin: HandleLogin) {
    this.emailValidator = emailValidator;
    this.handleLogin = handleLogin;
  }

  @logErrors()
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const requiredFields = [
        'email',
        'password',
      ];

      for (const field of requiredFields) {
        if (!httpRequest.body[field]) {
          return badRequest(new MissingParamErro(field));
        }
      }

      const { email } = httpRequest.body;

      if (!this.emailValidator.isValid(email)) { return badRequest(new InvalidParamError('email')); }

      logger.log('info', 'Users logged in');
      return ok(await this.handleLogin.login(httpRequest.body));
    } catch (error) {
      switch (error.message) {
        case 'INVALID_PASSWORD':
        case 'ACCOUNT_NOT_FOUND':
          return unauthorized(new InvalidPasswordOrEmail());
        default: {
          logger.log('error', error);
          return serverError(error);
        }
      }
    }
  }
}
