import { GetUsersFromFakeApi } from '@/domain/usecases/get-users-from-fake-api';
import { logger } from '@/main/config/logger';
import { serverError } from '@/presentation/helpers/http-helper';
import { Controller, HttpResponse } from '../../protocols';

export class GetUsersFromFakeApiController implements Controller {
  private readonly getUsersFromFakeApi: GetUsersFromFakeApi

  constructor(getUsersFromFakeApi: GetUsersFromFakeApi) {
    this.getUsersFromFakeApi = getUsersFromFakeApi;
  }

  async handle(): Promise<HttpResponse> {
    try {
      const response = await this.getUsersFromFakeApi.get();

      return {
        statusCode: 200,
        body: response,
      };
    } catch (error) {
      logger.log('error', error);
      switch (error.message) {
        case 'REQUEST_FAIL':
          return serverError(error);
        default:
          return serverError(error);
      }
    }
  }
}
