import { logErrors } from '@/utils/decorators/logger-decorator';
import { DeleteAccount } from '../../../domain/usecases/delete-account';
import { MissingParamErro } from '../../errors';
import { AccountNotFoundError } from '../../errors/account-not-found-error';
import { badRequest, ok, serverError } from '../../helpers/http-helper';
import { Controller, HttpRequest, HttpResponse } from '../../protocols';
import { logger } from '@/main/config/logger';

export class DeleteUserController implements Controller {
  private deleteAccount: DeleteAccount

  constructor(deleteAccount: DeleteAccount) {
    this.deleteAccount = deleteAccount;
  }

  @logErrors()
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      if (!httpRequest.query.id) throw new Error('INVALID_PARAM');

      await this.deleteAccount.delete(httpRequest.query.id);
      logger.log('warn', `User deleted: ${httpRequest.query.id}`);
      return ok('deleted');
    } catch (error) {
      switch (error.message) {
        case 'ACCOUNT_NOT_FOUND': {
          logger.log('warn', `Account not found: ${httpRequest.query.id}`);
          return badRequest(new AccountNotFoundError());
        }
        case 'INVALID_PARAM':
          return badRequest(new MissingParamErro('id'));
        default: {
          logger.log('error', error);
          return serverError(error);
        }
      }
    }
  }
}
