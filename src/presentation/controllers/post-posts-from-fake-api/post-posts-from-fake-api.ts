import { Controller, HttpRequest, HttpResponse } from '../../protocols';
import { serverError } from '../../helpers/http-helper';
import { PostPostsFromFakeApi } from '../../../domain/usecases/post-posts-from-fake-api';
import { SavePost } from '../../../domain/usecases/save-post';
import { logger } from '@/main/config/logger';

export class PostPostsFromFakeApiController implements Controller {
  private readonly postPostsFromFakeApi: PostPostsFromFakeApi

  private readonly savePost: SavePost

  constructor(postPostsFromFakeApi: PostPostsFromFakeApi, savePost: SavePost) {
    this.postPostsFromFakeApi = postPostsFromFakeApi;
    this.savePost = savePost;
  }

  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const response = await this.postPostsFromFakeApi.post(httpRequest);

      await this.savePost.post(httpRequest.body);

      return {
        statusCode: 200,
        body: response,
      };
    } catch (error) {
      logger.log('error', error);
      switch (error.message) {
        case 'REQUEST_FAIL':
          return serverError(error);
        default:
          return serverError(error);
      }
    }
  }
}
