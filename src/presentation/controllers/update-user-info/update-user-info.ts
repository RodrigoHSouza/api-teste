import { logErrors } from '@/utils/decorators/logger-decorator';
import { UpdateAccount } from '../../../domain/usecases/update-account';
import { InvalidParamError, NotLoggedIn } from '../../errors';
import {
  badRequest, ok, serverError, unauthorized,
} from '../../helpers/http-helper';
import { Controller, HttpRequest, HttpResponse } from '../../protocols';
import { logger } from '@/main/config/logger';

export class UpdateUserInfoController implements Controller {
  private readonly updateAccount: UpdateAccount

  constructor(updateAccount: UpdateAccount) {
    this.updateAccount = updateAccount;
  }

  @logErrors()
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const notAllowedParams = [
        'password',
        '_id',
        'id',
      ];

      for (const field of notAllowedParams) {
        if (httpRequest.body[field]) {
          delete httpRequest.body[field];
        }
      }

      const accountId = httpRequest.body.token.id;
      delete httpRequest.body.token;

      const accountUpdated = await this.updateAccount.update(
        accountId,
        httpRequest.body,
      );

      logger.log('info', `Update account info: ${accountId}`);
      return ok(accountUpdated);
    } catch (error) {
      switch (error.message) {
        case 'ACCOUNT_NOT_FOUND': {
          logger.log('warn', `Account not found: ${httpRequest.body.token.id}`);
          return unauthorized(new NotLoggedIn());
        }
        case 'INVALID_PARAM':
          return badRequest(new InvalidParamError('password not allowed'));
        default: {
          logger.log('error', error);
          return serverError(error);
        }
      }
    }
  }
}
