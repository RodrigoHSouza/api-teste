import { VerifyToken } from '../../domain/usecases/verify-token';
import { NotLoggedIn } from '../errors';
import { serverError, unauthorized } from '../helpers/http-helper';
import { GetToken } from '../protocols/get-token';
import { Middleware } from '../protocols/middleware';

export class VerifyTokenMiddleware implements Middleware {
  private readonly getToken: GetToken

  private readonly verifyToken: VerifyToken

  constructor(getToken: GetToken, verifyToken: VerifyToken) {
    this.getToken = getToken;
    this.verifyToken = verifyToken;
  }

  async handle(...[httpRequest, next]: Middleware.Params): Middleware.Result {
    try {
      const token = this.getToken.getToken(httpRequest.headers);
      const decodedToken = await this.verifyToken.verify(token);

      httpRequest.body.token = decodedToken.data;

      return next();
    } catch (error) {
      switch (error.message) {
        case 'HEADER_NOT_FOUND':
        case 'INVALID_HEADER':
        case 'invalid token':
        case 'jwt expired':
          return unauthorized(new NotLoggedIn());
        default:
          return serverError(error);
      }
    }
  }
}
