import { GetUserInfoById } from '../../domain/usecases/get-user-info-by-id';
import { NotLoggedIn } from '../errors';
import { NecessaryPermissionsError } from '../errors/necessary-permissions-error';
import { serverError, unauthorized } from '../helpers/http-helper';
import { Middleware } from '../protocols/middleware';

export class VerifyAdminMiddleware implements Middleware {
  private readonly getUserInfo: GetUserInfoById

  constructor(getUserInfo: GetUserInfoById) {
    this.getUserInfo = getUserInfo;
  }

  async handle(...[httpRequest, next]: Middleware.Params): Middleware.Result {
    try {
      const { admin } = await this.getUserInfo.getUserInfoById(httpRequest.body.token.id);

      if (!admin) return unauthorized(new NecessaryPermissionsError());

      return next();
    } catch (error) {
      switch (error.message) {
        case 'ACCOUNT_NOT_FOUND':
          return unauthorized(new NotLoggedIn());
        default:
          return serverError(error);
      }
    }
  }
}
