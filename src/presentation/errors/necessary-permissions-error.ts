export class NecessaryPermissionsError extends Error {
  constructor() {
    super('You don\'t have the necessary permissions.');
    this.name = 'NecessaryPermissions';
  }
}
