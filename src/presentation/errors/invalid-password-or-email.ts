export class InvalidPasswordOrEmail extends Error {
  constructor() {
    super('Invalid password or email');
    this.name = 'Unauthorized';
  }
}
