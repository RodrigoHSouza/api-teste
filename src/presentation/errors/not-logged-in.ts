export class NotLoggedIn extends Error {
  constructor() {
    super('User not logged in');
    this.name = 'Unauthorized';
  }
}
