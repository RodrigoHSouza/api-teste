import apm from 'elastic-apm-node';
import { ServerError } from '../errors';
import { HttpResponse } from '../protocols';

export const badRequest = (error: Error): HttpResponse => {
  apm.captureError(error);
  return {
    statusCode: 400,
    body: error.message,
  };
};

export const serverError = (error: Error): HttpResponse => {
  apm.captureError(error);
  return {
    statusCode: 500,
    body: new ServerError(error.stack!),
  };
};

export const ok = (data: any): HttpResponse => ({
  statusCode: 200,
  body: data,
});

export const unauthorized = (error: Error): HttpResponse => ({
  statusCode: 401,
  body: error.message,
});
