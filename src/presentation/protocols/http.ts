export interface HttpResponse {
  statusCode: number
  body: any
  headers?: any
}

export interface HttpRequest {
  headers: any
  body?: any
  query: any,
  url: string
}
