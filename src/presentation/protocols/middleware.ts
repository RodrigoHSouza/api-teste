import { HttpRequest, HttpResponse } from '.';

export interface Middleware {
  handle: (...params: Middleware.Params) => Middleware.Result
}

export namespace Middleware {
  export type Params = [httpRequest: HttpRequest, next: Function];
  export type Result = Promise<HttpResponse>;
}
