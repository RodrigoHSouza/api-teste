export interface GetToken {
  getToken: (data: any) => string
}
