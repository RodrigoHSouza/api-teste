# BUILD STAGE
FROM node:lts-slim as builder

WORKDIR /home/node/app

COPY package.json .
RUN yarn install

COPY --chown=node:node . .
RUN yarn build

# RUN STAGE
FROM node:lts-slim

WORKDIR /home/node/app

COPY package.json .
RUN yarn global add typescript
RUN yarn global add ts-node
RUN yarn global add tslib @types/node
RUN yarn install --production=true

USER node

COPY --from=builder /home/node/app .

EXPOSE 3000

CMD [ "yarn", "server:start"]
